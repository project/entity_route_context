<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_route_context\Kernel;

use Drupal\block\Entity\Block;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\entity_route_context_block_test\Plugin\Block\EntityRouteContextBlockTestBlock;
use Drupal\entity_route_context_block_test\Plugin\Condition\EntityRouteContextBlockTestCondition;
use Drupal\KernelTests\KernelTestBase;

/**
 * General tests for entity route context.
 *
 * @group entity_route_context
 */
final class EntityRouteContextTest extends KernelTestBase {

  protected static $modules = [
    'system',
    'entity_route_context',
    'entity_test',
    'block',
    'entity_route_context_block_test',
  ];

  /**
   * Tests block access is allowed and cacheable.
   */
  public function testBlockAccess(): void {
    $block = Block::create([
      'id' => 'my_block',
      'plugin' => EntityRouteContextBlockTestBlock::PLUGIN_ID,
      'status' => TRUE,
    ]);
    $block->setVisibilityConfig(EntityRouteContextBlockTestCondition::PLUGIN_ID, [
      'id' => EntityRouteContextBlockTestCondition::PLUGIN_ID,
      'negate' => FALSE,
      'context_mapping' => [
        'entity_test_1' => '@entity_route_context.entity_route_context:canonical_entity:entity_test',
      ],
    ]);

    /** @var \Drupal\Core\Access\AccessResult $access */
    $access = $block->access('view', NULL, TRUE);
    $this->assertInstanceOf(AccessResultForbidden::class, $access);
    // \Drupal\block\BlockAccessControlHandler::checkAccess will set max age to
    // 0 if the context wasn't supplied.
    $this->assertEquals(-1, $access->getCacheMaxAge());
  }

}
