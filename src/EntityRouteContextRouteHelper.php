<?php

declare(strict_types=1);

namespace Drupal\entity_route_context;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Symfony\Component\Routing\Route;

/**
 * Route helper.
 *
 * Designed to assist determining which routes are owned by a particular entity
 * type by way of link templates.
 */
final class EntityRouteContextRouteHelper implements EntityRouteContextRouteHelperInterface {

  /**
   * Cache bin CID to store route/link template map.
   */
  protected const ENTITY_ROUTE_CONTEXT_MAP = 'entity_route_context:link_template_map';

  /**
   * Entity types keyed by route name. Or NULL if not yet built.
   *
   * @var array|null
   */
  protected ?array $routes;

  /**
   * Route names keyed by entity type. Or NULL if not yet built.
   *
   * @var array|null
   */
  protected ?array $routesByEntityType;

  /**
   * Constructs a new EntityRouteContextRouteHelper.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected RouteProviderInterface $routeProvider,
    protected CacheBackendInterface $cache,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function getAllRouteNames(): array {
    if (!isset($this->routes)) {
      $this->primeCaches();
    }

    return \array_map(static function (array $value): string {
      return $value[0];
    }, $this->routes);
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteNames(string $entityTypeId): array {
    if (isset($this->routesByEntityType[$entityTypeId])) {
      return $this->routesByEntityType[$entityTypeId];
    }

    if (!isset($this->routes)) {
      $this->primeCaches();
    }

    $routes = \array_filter(
      $this->routes,
      static function (array $value) use ($entityTypeId): bool {
        return $value[0] === $entityTypeId;
      },
    );

    $this->routesByEntityType[$entityTypeId] = [];
    foreach ($routes as $routeName => [, $linkTemplate]) {
      $this->routesByEntityType[$entityTypeId][$linkTemplate] = $routeName;
    }

    return $this->routesByEntityType[$entityTypeId];
  }

  /**
   *
   */
  public function getEntityTypeId(string $routeName): ?string {
    if (!isset($this->routes)) {
      $this->primeCaches();
    }

    return $this->routes[$routeName][0] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLinkTemplateByRouteMatch(RouteMatchInterface $routeMatch): ?array {
    if (!isset($this->routes)) {
      $this->primeCaches();
    }

    $routeName = $routeMatch->getRouteName();
    if (!\is_string($routeName)) {
      return NULL;
    }
    return $this->routes[$routeName] ?? NULL;
  }

  /**
   * Gets or computes entity route map.
   */
  protected function primeCaches(): void {
    /** @var object{data: array}|false $item */
    $item = $this->cache->get(static::ENTITY_ROUTE_CONTEXT_MAP);
    if (FALSE !== $item) {
      $this->routes = $item->data ?? [];
      return;
    }

    // Remove ignore after https://www.drupal.org/project/drupal/issues/3161129.
    /** @var \Generator<\Symfony\Component\Routing\Route> $allRoutes */
    $allRoutes = $this->routeProvider->getAllRoutes();
    $pathByRouteName = \array_map(
      static function (Route $route) {
        return $route->getPath();
      },
      \iterator_to_array($allRoutes),
    );

    $routes = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entityType) {
      foreach ($entityType->getLinkTemplates() as $linkTemplateKey => $linkTemplate) {
        $key = \array_search($linkTemplate, $pathByRouteName, TRUE);
        if ($key !== FALSE) {
          $routes[$key] = [$entityType->id(), $linkTemplateKey];
        }
      }
    }

    // Same tag used by entity type definitions in 'discovery' bin.
    $tags = ['entity_types', 'routes'];
    $this->cache->set(static::ENTITY_ROUTE_CONTEXT_MAP, $routes, Cache::PERMANENT, $tags);
    $this->routes = $routes;
  }

}
